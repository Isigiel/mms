function updatePreview(textArea){
    $("#"+textArea.attr("data-previewto")).html(markdown.toHTML(textArea.val()));
}

$(document).ready(function(){
    $(".textarea-preview").keyup(function(e){
        updatePreview($(e.target));
    }).each(function(i,el){
        updatePreview($(el));
    });
});
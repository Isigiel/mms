<?php

class IndexController extends BaseController {
    public function getIndex ()
    {
        return View::make('site')->with('data', Fetch::site('first'));
    }
    
    public function postLogin ()
    {
        if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password', Input::get('remember')))))
        {
            $data=Fetch::site('first');
            $data['alert'][1] = array('type'=>'success','text'=>'Login successful.');
            return Redirect::to('/')->with('data', $data);
        }
        else
        {
            return Redirect::to('login')->with('login_errors', true);
        }
    }
    
    public function getLogin ()
    {
            $data=Fetch::site('first');
            $data['login']=true;
            return View::make('site')->with('data', $data);
    }
    
    public function getSite($site)
    {
        return View::make('site')->with('data', Fetch::site($site));
    }
    
    public function getBlog ()
    {
        return View::make('blog')->with('blog', Fetch::blog);
    }
}
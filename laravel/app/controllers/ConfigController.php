<?php
use \Michelf\MarkdownExtra;

class ConfigController extends BaseController {
    
    public function __construct()
    {
        $this->beforeFilter('auth');
    }
    
    
    public function getIndex ()
    {
        return View::make('config')->with('data',Fetch::config());
    }
    
    public function postMain ()
    {
        $config=Setting::first();
        $config->title=Input::get('title');
        $config->theme=Input::get('theme');
        $config->copyright=Input::get('copyright');
        $config->blog=Input::get('blog');
        $config->download=Input::get('down');
        if (Input::get('autopost')=='on') 
        {$config->autopost=true;}
        else
        {$config->autopost=false;}
        $config->save();
        $data = Fetch::config();
        $data['alert'][1] = array('type'=>'success','text'=>'Data was successfully saved.');
        return View::make('config')->with('data',$data);
    }
    
    public function postSite ($title)
    {
        if ($title == 'new')
        {
            $site=new Site;
        }
        else
        {
            $site=Site::where('id',$title)->first();
        }
        
        $site->title=Input::get('title');
        $site->headline=Input::get('headline');
        $site->subtitle=Input::get('subtitle');
        $site->nobbc=Input::get('content');
        $site->content=MarkdownExtra::defaultTransform(Input::get('content'));
        $site->save();
        $data = Fetch::config();
        $data['alert'][1] = array('type'=>'success','text'=>'Data was successfully saved.');
        return View::make('config')->with('data',$data);
    }
    
    public function getDelete ($id)
    {
        Site::destroy($id);
        $data = Fetch::config();
        $data['alert'][1] = array('type'=>'danger','text'=>'Site was deleted.');
        return View::make('config')->with('data',$data);
    }
    
    public function postBlog ($id)
    {
        if ($id == 'new')
        {
            $post=new Post;
        }
        else
        {
            $post=Post::where('id',$id)->first();
        }
        
        $post->title=Input::get('title');
        $post->content=MarkdownExtra::defaultTransform(Input::get('content'));
        $post->nomk=Input::get('content');
        $post->tags=Input::get('tags');
        $post->save();
        $data = Fetch::config();
        $data['alert'][1] = array('type'=>'success','text'=>'Data was successfully saved.');
        return View::make('config')->with('data',$data);
    }
    
    
    public function getDpost ($id)
    {
        Post::destroy($id);
        $data = Fetch::config();
        $data['alert'][1] = array('type'=>'danger','text'=>'Post was deleted.');
        return View::make('config')->with('data',$data);
    }
    
    public function postDownload()
    {
        $download=new Download;
        $download->title=Input::get('title');
        $download->version=Input::get('version');
        $download->mcversion=Input::get('mcversion');
        $download->link=Input::get('link');
        $download->save();
        
        $data = Fetch::config();
        
        
        if ($data['autopost'])
        {
        $post=new Post;
        $post->title="Release(V".Input::get('version').")";
        $post->nomk="Version ".Input::get('version')." for Minecraft version ".Input::get('mcversion')." was just released, get it [here](".Input::get('link').")!";
        $post->content=MarkdownExtra::defaultTransform($post->nomk);
        $post->tags="Release, ".Input::get('mcversion');
        $auto=true;
        $post->save();
        }
        
        $data = Fetch::config();
        
        $data['alert'][1] = array('type'=>'success','text'=>'Download was successfully saved.');
        
        if ($auto)
        {
        $data['alert'][2] = array('type'=>'success','text'=>'Automatic post was generated.');
        }
        
        return View::make('config')->with('data',$data);
    }
    
    public function getDdown ($id)
    {
        Download::destroy($id);
        
        $data = Fetch::config();
        $data['alert'][1] = array('type'=>'danger','text'=>'Download was deleted.');
        return View::make('config')->with('data',$data);
    }
    
}
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('logout', function() {
 // logout from the system
    Auth::logout();
    $data=Fetch::site('first');
    $data['alert'][1] = array('type'=>'success','text'=>'Login successful.');
    return Redirect::to('/')->with('data', $data);
});


Route::controller('config','ConfigController');

Route::controller('blog','BlogController');

Route::controller('/','IndexController');

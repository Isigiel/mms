<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// $this->call('UserTableSeeder');#
		$this->call('UserTableSeeder');

        $this->command->info('User table seeded!');
        
        $this->call('SiteTableSeeder');

        $this->command->info('Site table seeded!');
        
        $this->call('SettingTableSeeder');

        $this->command->info('Setting table seeded!');
        
        $this->call('PostTableSeeder');
        
        $this->command->info('Post table seeded!');
    }

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();
        User::create(array('email' => 'lukas@mms.com','password'=>Hash::make('lukas'),'username'=>'Lukas'));
        User::create(array('email' => 'archadia@mms.com','password'=>Hash::make('archadia'),'username'=>'Archadia'));
        User::create(array('email' => 'jadenn@mms.com','password'=>Hash::make('jadenn'),'username'=>'Jadenn'));
        User::create(array('email' => 'nino@mms.com','password'=>Hash::make('nino'),'username'=>'Plok'));
        User::create(array('email' => 'lapito@mms.com','password'=>Hash::make('lapito'),'username'=>'Lapito'));
        User::create(array('email' => 'pixlepix@mms.com','password'=>Hash::make('pixlepix'),'username'=>'Pixlepix'));
        User::create(array('email' => 'abrar@mms.com','password'=>Hash::make('abrar'),'username'=>'Abrar'));
        User::create(array('email' => 'mod@mms.com','password'=>Hash::make('minecraft'),'username'=>'Modder'));
    }

}

class SiteTableSeeder extends Seeder {

    public function run()
    {
        DB::table('sites')->delete();

Site::create(array('title' => 'MMS','content'=>'<h2>Features</h2>

<ul>
<li>Use Markdown nearly everywhere</li>
<li>Easy Backend, made for the mods</li>
<li>Designed for mods, no unwanted features</li>
<li>Easy Markdown live preview</li>
<li>Integrated Blog feature</li>
<li>Easy to use Backend</li>
<li>New Features nearly every day</li>
</ul>','nobbc'=>'##Features##
* Use Markdown nearly everywhere
* Easy Backend, made for the mods
* Designed for mods, no unwanted features
* Easy Markdown live preview
* Integrated Blog feature
* Easy to use Backend
* New Features nearly every day'));

Site::create(array('title' => 'Blog','content'=>'<p>Hello, this is a small exaple for the mod function of mms.</p>

<p>The site is optional and configured in the backend</p>','nobbc'=>'Hello, this is a small exaple for the mod function of mms.

The site is optional and configured in the backend'));
    }

}

class SettingTableSeeder extends Seeder {

    public function run()
    {
        DB::table('settings')->delete();

        Setting::create(array('title' => 'MMS', 'blog'=>'2', 'copyright'=>'MMXIII Isigiel'));
    }

}

class PostTableSeeder extends Seeder {

    public function run()
    {
        DB::table('posts')->delete();

Post::create(array('title'=>'Alpha', 'tags'=>'alpha, preview, release', 'content'=>'<h4>Early Alpha release</h4>

<p>After quite some work, I decided to release an early alpha for people who want to test mms.</p>

<p>If many people are testing this it could become a cool thing made by all of you.</p>', 'nomk'=>'####Early Alpha release####
After quite some work, I decided to release an early alpha for people who want to test mms.

If many people are testing this it could become a cool thing made by all of you.'));
    }
}
<?php
    class Fetch
    {
        public static function settings()
        {
            $settings=Setting::first();
            $return=array();
            $return['title']=$settings->title;
            $return['copyright']=$settings->copyright;
            $return['theme']=$settings->theme;
            $return['blogsite']=$settings->blog;
            $return['downsite']=$settings->download;
            $return['autopost']=$settings->autopost;
            return $return;
        }
        
        public static function down()
        {
            $downloads=Download::all();
            $i=0;
            $return[0]=false;
            foreach ($downloads as $download)
            {    
            $return[$i]['title']=$download->title;
            $return[$i]['link']=$download->link;
            $return[$i]['version']=$download->version;
            $return[$i]['mcversion']=$download->mcversion;
            $return[$i]['id']=$download->id;
            $i++;
            }
            return $return;
        }
        
        public static function nav()
        {
            $sites=Site::lists('title');
            $ids=Site::lists('id');
            $i=0;
            foreach ($sites as $site)
            {
                $return[$i]['title']=$site;
                $return[$i]['id']=$ids[$i];
                $i++;
            }
            return $return;
        }
        
        public static function blog()
        {
            $blog=Post::all();
            
            $i=count($blog)-1;
            $return= array();
            $return[0]=false;
            foreach ($blog as $entry)
            {
                $return[$i]['title']=$entry->title;
                $return[$i]['id']=$entry->id;
                $return[$i]['content']=$entry->content;
                $return[$i]['nomk']=$entry->nomk;
                $return[$i]['tagse']=0;
                $return[$i]['tagse']=explode(",",$entry->tags);
                $return[$i]['tags']=$entry->tags;
                $i--;
            }
            
            return $return;
        }
        
        public static function site($title)
        {
            if ($title=='all')
            {
                $sites=Site::all();
                $data=array();
                $data=self::settings();
                
                $i=0;
                foreach ($sites as $site)
                {
                    $data['page'][$i]['site']=$site->title;
                    $data['page'][$i]['content']=$site->content;
                    $data['page'][$i]['nobbc']=$site->nobbc;
                    $data['page'][$i]['headline']=$site->headline;
                    $data['page'][$i]['subtitle']=$site->subtitle;
                    $data['page'][$i]['id']=$site->id;
                    $i++;
                }
                
                $data['site']=false;
                $data['nav']=self::nav();
                $data['alert']=null;
                $data['login']=false;
                $data['down']=self::down();
                return $data;
                
            }
            else
            {
            
                if ($title == 'first')
                {
                    $site=Site::first();
                }
                else
                {
                    $site=Site::where('id',$title)->first();
                }
            
            
            
            $data=array();
            $data=self::settings();
            $data['site']=$site->title;
            $data['content']=$site->content;
            $data['nobbc']=$site->nobbc;
            $data['headline']=$site->headline;
            $data['subtitle']=$site->subtitle;
            $data['id']=$site->id;
            $data['nav']=self::nav();
            $data['alert']=null;
            $data['login']=false;
            $data['blog']=self::blog();
            $data['down']=self::down();
            return $data;
            }
        }
        
        public static function config()
        {
            $data=self::site('all');
            $data['blog']=self::blog();
            return $data;
        }
        
        
    }
@extends ('base.base')

@section('body')


<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	<span class="sr-only">Toggle navigation</span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	</button>
	<a class="navbar-brand" href="{{ URL::to('/') }}">{{$data['title']}} <span class="label label-success">Open Alpha</span></a>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


	<ul class="nav navbar-nav">
	
	    @foreach ($data['nav'] as $nav)
	    <li 
	    @if ($nav['title'] == $data['site'])
	    class="active"
	    @endif
	    >
	    <a href="{{URL::to('/')}}/site/{{$nav['id']}}">{{$nav['title']}}</a>
	    @endforeach
	</ul>
	
	
	<ul class="nav navbar-nav navbar-right">
	@if ( Auth::guest() )
            <li><a href="#" data-toggle="modal" data-target="#login">Login</a></li>
        @else
        <li>{{ HTML::link('config', 'Manage') }}</li>
        <li>{{ HTML::link('logout', 'Logout') }}</li>
    @endif
    </ul>
</div>

</nav>


<div class="row">
    <div class="col-md-10 col-md-offset-1">
    @if ($data['alert']!=null)
    @foreach ($data['alert'] as $alert)
        <div id="alert" class="alert alert-{{$alert['type']}} fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{$alert['text']}}
        </div>
    @endforeach
    @endif

@yield ('content')

    </div>
</div>
@if ( Auth::guest() )
<!-- Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Log in</h4>
      </div>
      <div class="modal-body">
      @if (Session::has('login_errors'))
            <div class="alert alert-danger">
                <span>Username or password incorrect.</span>
            </div>
        @endif
        {{ Form::open(array('url' => 'login')) }}
        
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Enter email">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
            </div>
            <div class="checkbox">
                <label>
                    <input name="remember" type="checkbox"> Remeber me
                </label>
            </div>
        

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Login</button>
        {{ Form::close() }}
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endif
@stop

@section ('script')
@if ($data['login'] && Auth::guest())
    <script>
        $(document).ready(function() {
            $('#login').modal('show')
        });
    </script>
@endif
<script>
        $(document).ready(function() {
            setTimeout(function (){
            $(".alert").fadeOut("slow")
         }, 3000);
        });
</script>
@yield('js')
@stop
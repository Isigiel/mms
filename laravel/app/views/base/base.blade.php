<!DOCTYPE html>
<html>
  <head>
    <title>{{$data['title']}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    @if ($data['theme']=='')
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">
    @else
    <!-- Custom Theme -->
    <link rel="stylesheet" href="{{$data['theme']}}">
    @endif
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="padding-top: 70px;">
    @yield('body')
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
    @yield('script')
    
    <br><br><br>
    <footer>
  <div class="well">
                <div class="row">
                    <div class="col-sm-6">
                        <div allign="left">
                            <small>© {{$data['copyright']}} | MMS V A-0.2</small>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div align="right">
                            <small>Design and Code by
                                <a target="_blank" href="http://bit.ly/Isigiel">Isigiel</a>
                            </small>
                        </div>
                    </div>
                </div>
            </div>
  </footer>
  </body>
  
</html>
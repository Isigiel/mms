@extends ('base.nav')

@section ('content')


<h1>Welcome to the MMS <small>Here you can change the look and content of your site</small></h1>
<hr>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#base" data-toggle="tab">Base</a></li>
  <li class="dropdown">
          <a href="#" id="sites" class="dropdown-toggle" data-toggle="dropdown">Sites<b class="caret"></b></a>
          <ul class="dropdown-menu" role="menu" aria-labelledby="sites">
          @foreach ($data['page'] as $page)
            <li><a href="#s{{$page['id']}}" tabindex="-1" data-toggle="tab">{{$page['site']}}</a></li>
          @endforeach
          </ul>
    </li>
  <li><a href="#newsite" data-toggle="tab">New Site</a></li>
  @if ($data['blog'][0]!=false)
  <li class="dropdown">
          <a href="#" id="posts" class="dropdown-toggle" data-toggle="dropdown">Posts<b class="caret"></b></a>
          <ul class="dropdown-menu" role="menu" aria-labelledby="sites">
          @foreach ($data['blog'] as $post)
            <li><a href="#p{{$post['id']}}" tabindex="-1" data-toggle="tab">{{$post['title']}}</a></li>
          @endforeach
          </ul>
    </li>
    @endif
  <li><a href="#newblog" data-toggle="tab">New Post</a></li>
  <li><a href="#download" data-toggle="tab">Downloads</a></li>
  
</ul>
<br>
<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="base">
  {{ Form::open(array('url' => 'config/main')) }}
        
            <div class="row"><div class="col-md-4">
            
            <div class="form-group">
                <label for="title">Title of the Website</label>
                <input type="text" id="title" class="form-control" name="title" value="{{$data['title']}}" placeholder="Title">
            </div>
            
            </div><div class="col-md-4">
            
            <div class="form-group">
                <label for="copyright">Set the Copyright below</label>
                <input type="text" id="copyright" class="form-control" name="copyright" value="{{$data['copyright']}}" placeholder="Copyright">
            </div>
            
            </div><div class="col-md-2">
            
            <div class="form-group">
                <label for="blogsite">Set the site your blog is visible on</label>
                <select name="blog" class="form-control">
                    <option value="no">no blog</option>
                    @foreach ($data['page'] as $page)
                    <option
                    @if ( $page['id'] == $data['blogsite'] )
                    selected="selected"
                    @endif
                     value="{{$page['id']}}"
                    >{{$page['site']}}</option>
                    @endforeach
                </select>
            </div>
            
            </div><div class="col-md-2">
            
            <div class="form-group">
                <label for="blogsite">Set your "Downloads" site</label>
                <select name="down" class="form-control">
                    <option value="no">no download page</option>
                    @foreach ($data['page'] as $page)
                    <option
                    @if ( $page['id'] == $data['downsite'] )
                    selected="selected"
                    @endif
                     value="{{$page['id']}}"
                    >{{$page['site']}}</option>
                    @endforeach
                </select>
            </div>
            
            </div></div>
            
            <div class="form-group">
                <label for="theme">Theme of your website <small><br>
                This is optional, leave blank if you don't want to use it.</small></label>
                <input type="text" id="theme" class="form-control" name="theme" value="{{$data['theme']}}" placeholder="Theme">
            </div>
            
            <div class="checkbox">
                <label>
                    <input @if ( $page['id'] == $data['downsite'] )
                    checked
                    @endif
                    name="autopost" type="checkbox"> Toggle auto post creation for new downloads
                </label>
            </div>
            
        <button type="submit" class="btn btn-success">Save</button>
        {{ Form::close() }}
    </div>
        
    @foreach ($data['page'] as $page)
        <div class="tab-pane" id="s{{$page['id']}}">
        {{ Form::open(array('url' => 'config/site/'.$page["id"])) }}
        
            <div class="row">
            <div class="col-md-4">
            
            <div class="form-group">
                <label for="title">Title of {{$page['site']}}</label>
                <input type="text" id="title" class="form-control" name="title" value="{{$page['site']}}"   placeholder="Title">
            </div>
            </div>
            <div class="col-md-4">
            <div class="form-group">
                <label for="title">Headline of {{$page['site']}}</label>
                <input type="text" id="title" class="form-control" name="headline" value="{{$page['headline']}}" placeholder="Headline">
            </div>
            </div>
            <div class="col-md-4">
            <div class="form-group">
                <label for="subtitle">Subtitle for {{$page['site']}}</label>
                <input type="text" id="subtitle" class="form-control" name="subtitle" value="{{$page['subtitle']}}" placeholder="Subtitle">
            </div>
            </div></div>
            <div class="form-group">
                <label for="Content">The Content of {{$page['site']}} <br><small>Feel free to use <a href="http://michelf.ca/projects/php-markdown/concepts/">Markdown</a></small></label>
                <textarea data-previewto="preview-{{$page['site']}}" class="textarea-preview form-control" name="content" id="content" rows="7">{{$page['nobbc']}}</textarea>
            </div>
            
            
        <button type="submit" class="btn btn-success">Save</button>
        @if ($data['nav'][0]['id']==$page['id'])
        <a class="btn btn-danger" disabled href="{{URL::to('/')}}/config/delete/{{$page['id']}}"><small>You can't delete this page!</small></a>
        @else
        <a class="btn btn-danger" href="{{URL::to('/')}}/config/delete/{{$page['id']}}">Delete</a>
        @endif
        
        <br><br><br>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Preview</h3>
            </div>
            <div class="panel-body">
                <div id="preview-{{$page['site']}}">
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
    @endforeach
  <div class="tab-pane" id="newsite">
  
  {{ Form::open(array('url' => 'config/site/new')) }}
  
        
            <div class="row">
            <div class="col-md-4">
            
            <div class="form-group">
                <label for="title">Title of the new site</label>
                <input type="text" id="title" class="form-control" name="title" placeholder="Title">
            </div>
            
            </div>
            <div class="col-md-4">
            
            <div class="form-group">
                <label for="title">Headline of the new site</label>
                <input type="text" id="title" class="form-control" name="headline" placeholder="Headline">
            </div>
            
            </div>
            <div class="col-md-4">
            
            <div class="form-group">
                <label for="subtitle">Subtitle for the new site</label>
                <input type="text" id="subtitle" class="form-control" name="subtitle" placeholder="Subtitle">
            </div>
            
            </div>
            </div>
            
            <div class="form-group">
                <label for="Content">The Content of the new site <br><small>Feel free to use <a href="http://michelf.ca/projects/php-markdown/concepts/">Markdown</a></small></label>
                <textarea data-previewto="preview-content" class="textarea-preview form-control" name="content" id="content" rows="7"></textarea>
            </div>
            

            
            
        <button type="submit" class="btn btn-success">Save</button>
    {{ Form::close() }}
        <br><br><br>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Preview</h3>
            </div>
            <div class="panel-body">
                <div id="preview-content">
                </div>
            </div>
        </div>
  
  </div>
  <div class="tab-pane" id="newblog">
  
  {{ Form::open(array('url' => 'config/blog/new')) }}
        
            <div class="row">
            <div class="col-md-4">
            
            <div class="form-group">
                <label for="title">Title of the new entry</label>
                <input type="text" id="title" class="form-control" name="title" placeholder="Title">
            </div>
            
            </div>
            <div class="col-md-4">
            
            <div class="form-group">
                <label for="title">Tags of the new entry <small>(seperate with comma)</small></label>
                <input type="text" id="title" class="form-control" name="tags" placeholder="tags">
            </div>
            
            </div>
            </div>
            
            <div class="form-group">
                <label for="Content">The Content of the new entry <br><small>Use not too much <a href="http://michelf.ca/projects/php-markdown/concepts/">Markdown</a> here :)</small></label>
                <textarea  data-previewto="preview-blog" class="textarea-preview form-control" name="content" id="content" rows="7"></textarea>
            </div>
            
            
        <button type="submit" class="btn btn-success">Save</button>
    {{ Form::close() }}
  
        <br><br><br>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Preview</h3>
            </div>
            <div class="panel-body">
                <div id="preview-blog">
                </div>
            </div>
        </div>
        </div>
        
        @foreach ($data['blog'] as $post)
        <div class="tab-pane" id="p{{$post['id']}}">
        {{ Form::open(array('url' => 'config/blog/'.$post["id"])) }}
        
            <div class="row">
            <div class="col-md-4">
            
            <div class="form-group">
                <label for="title">Title of {{$post['title']}}</label>
                <input type="text" id="title" class="form-control" name="title" value="{{$post['title']}}"   placeholder="Title">
            </div>
            </div>
            <div class="col-md-4">
            <div class="form-group">
                <label for="title">Tags of the new entry <small>(seperate with comma)</small></label>
                <input type="text" id="title" class="form-control" name="tags" placeholder="tags" value="{{$post['tags']}}">
            </div>
            </div>
            </div>
            <div class="form-group">
                <label for="Content">The Content of {{$post['title']}} <br><small>Feel free to use <a href="http://michelf.ca/projects/php-markdown/concepts/">Markdown</a></small></label>
                <textarea data-previewto="preview-{{$post['title']}}" class="textarea-preview form-control" name="content" id="content" rows="7">{{$post['nomk']}}</textarea>
            </div>
            
            
        <button type="submit" class="btn btn-success">Save</button>
        <a class="btn btn-danger" href="{{URL::to('/')}}/config/dpost/{{$post['id']}}">Delete</a>
        
        <br><br><br>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Preview</h3>
            </div>
            <div class="panel-body">
                <div id="preview-{{$post['title']}}">
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
    @endforeach
    
    <div class="tab-pane" id="download">
    
    <table class="table table-hover">
    <tr>
        <td>Title</td>
        <td>Version</td>
        <td>MC-Version</td>
        <td>Link</td>
    </tr>
    <tr>
    {{ Form::open(array('url' => 'config/download')) }}
        <td><input type="text" id="title" class="form-control" name="title" placeholder="Title"></td>
        <td><input type="text" id="title" class="form-control" name="version" placeholder="Version"></td>
        <td><input type="text" id="title" class="form-control" name="mcversion" placeholder="MC-Version"></td>
        <td><div class="row"><div class="col-md-10">
            <input type="text" id="title" class="form-control" name="link" placeholder="Link"></div><div class="col-md-2">
            <button class="btn btn-success btn-sm" type="submit">Save</button></div></div>
        </td>
    {{ Form::close() }}
    </tr>
    
    @if ($data['down'][0]!=false)
    @foreach ($data['down'] as $download)
    <tr>
        <td>{{$download['title']}}</td>
        <td>{{$download['version']}}</td>
        <td>{{$download['mcversion']}}</td>
        <td>
            <div class="row"><div class="col-md-10">
            {{$download['link']}}
            </div><div class="col-md-2">
            <a class="btn btn-danger btn-sm" href="{{URL::to('/')}}/config/ddown/{{$download['id']}}">Delete</a></div></div>
        </td>
    </tr>
    @endforeach
    @endif
    
    </table>
    </div>
  
  </div>
  </div>
@stop

@section('js')
<script src="/mms/laravel/public/js/markdown.min.js"></script>
<script src="/mms/laravel/public/js/config.js"></script>
  @stop
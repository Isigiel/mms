@extends ('base.nav')

@section ('content')
<h1> {{$data['headline']}} <small>{{$data['subtitle']}}</small></h1>
<hr>
    {{$data['content']}}
    
    
@if ($data['id'] == $data['blogsite'] && $data['blog'][0] != false)
<hr>
<br>

@if (Auth::guest()==false)
{{ Form::open(array('url' => 'config/blog/new')) }}
<div class="panel panel-default">
    <div class="panel-heading">
            <div class="row">
            <div class="col-md-6">
                <h3 class="panel-title"><div class="form-group">
                <label for="title">Title of the new entry</label>
                <input type="text" id="title" class="form-control" name="title" placeholder="Title">
            </div></h3>
                </div>
                <div class="col-md-6">
                <div class="pull-right">
                    <div class="form-group">
                <label for="title">Tags of the new entry <small>(seperate with comma)</small></label>
                <input type="text" id="title" class="form-control" name="tags" placeholder="tags">
            </div>
                </div>
                </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                <label for="Content">The Content of the new entry <br><small>Use not too much <a href="http://michelf.ca/projects/php-markdown/concepts/">Markdown</a> here :)</small></label>
                <textarea  data-previewto="preview-blog" class="textarea-preview form-control" name="content" id="content" rows="4"></textarea>
                <br>
                <button type="submit" class="btn btn-success">Save</button>
            </div>
            </div>
        </div>
         {{ Form::close() }}
@endif



@foreach ($data['blog'] as $post)
        <div class="panel panel-default">
            <div class="panel-heading">
            <div class="row">
            <div class="col-md-6">
                <h3 class="panel-title">{{$post['title']}}</h3>
                </div>
                <div class="col-md-6">
                <div class="pull-right">
                    @foreach ($post['tagse'] as $tag)
                    <span class="label label-info">{{$tag}}</span>
                    @endforeach
                </div>
                </div>
                </div>
            </div>
            <div class="panel-body">
                {{$post['content']}}
            </div>
        </div>
@endforeach
    
@endif

@if ($data['id'] == $data['downsite'] && $data['down'][0] != false)
<hr>
<table class="table table-hover">
    <tr>
        <td>Title</td>
        <td>Version</td>
        <td>MC-Version</td>
        <td>Link</td>
    </tr>
@foreach ($data['down'] as $download)
        <tr>
        <td>{{$download['title']}}</td>
        <td>{{$download['version']}}</td>
        <td>{{$download['mcversion']}}</td>
        <td>
            <a class="btn btn-success btn-block" href="{{$download['link']}}">Download</a></div></div>
        </td>
    </tr>
@endforeach
</table>
@endif
    


@stop